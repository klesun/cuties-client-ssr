# cuties-client-ssr

Server Side Rendering of our web pages using nodejs. Adds page-specific metatags for SEO, like cutie name/description or presale page preview card. Needed to separate client deploy process from server. To be used instead of HtmlRenderController.

