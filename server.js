import * as http from 'http';

const HTTP_PORT = 40354;

function handleRequest(req, res) {
    res.statusCode = 501;
    res.end("To be implemented");
}

/**
 * this endpoint is supposed to get invoked for the
 * index.html request by our SPA - it adds SEO metatags
 */
async function serve() {
    http
        .createServer(handleRequest)
        .listen(HTTP_PORT, '0.0.0.0', () => {
            console.log('listening cuties-client-ssr requests on http://localhost:' + HTTP_PORT);
        });
}

serve().catch(exc => {
    console.error('Failed to start cuties-client-ssr server', exc);
    process.exit(1);
});
